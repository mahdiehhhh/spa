import Dashboard from '../../pages/Dashboard.js'
import Posts from '../../pages/Posts.js'
import Products from '../../pages/Products.js'
import NotFound from '../../pages/NotFound.js'
// take routes
function router() {

    const routes = [{
            path: '/',
            view: Dashboard
        },

        {
            path: '/product',
            view: Products
        },

        {
            path: '/posts',
            view: Posts
        },
    ];

    const locationPath = routes.map((item) => {
        return {
            route: item,
            isMatch: location.pathname === item.path,
        }
    });
    let match = locationPath.find((route) => route.isMatch);

    if (!match) {
        match = {
            routes: {
                path: '/not-found',
                view: NotFound,
                isMatch: true
            }
        }
    }

    document.querySelector('#app').innerHTML = match.route.view()
}

function newUrl(url) {
    history.pushState(null, null, url);
    router()

}

window.addEventListener('popstate', router)

// SIDEBAR TOGGLER :
const sidebarToggler = document.querySelector(".sidebar-toggler");
const sidebar = document.querySelector(".nav");
const root = document.documentElement;
sidebarToggler.addEventListener("click", () => {
    sidebar.classList.toggle("mini-sidebar");
    if (sidebar.classList.contains("mini-sidebar")) {
        root.style.setProperty("--nav-width", 50 + "px");
    } else {
        root.style.setProperty("--nav-width", 250 + "px");
    }
});

document, addEventListener('DOMContentLoaded', () => {
    document.body.addEventListener('click', (e) => {
        if (e.target.hasAttribute('data-link')) {
            e.preventDefault();
            newUrl(e.target.href)
        }
    })
    router()
});